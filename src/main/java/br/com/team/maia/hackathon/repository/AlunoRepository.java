package br.com.team.maia.hackathon.repository;

import br.com.team.maia.hackathon.model.Aluno;
import org.springframework.data.repository.CrudRepository;


/**
 * Created by kielsonzinn on 19/05/17.
 */
public interface AlunoRepository extends CrudRepository<Aluno, Integer> {

}
