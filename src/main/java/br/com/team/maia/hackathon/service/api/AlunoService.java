package br.com.team.maia.hackathon.service.api;

import br.com.team.maia.hackathon.model.Aluno;

/**
 * Created by kielsonzinn on 19/05/17.
 */
public interface AlunoService {

    Iterable<Aluno> findAll();

    Aluno insert(Aluno aluno);

    Aluno search(Integer id );

    Aluno update(Aluno aluno);

    void delete( Integer id );

}

