define(function(require) {
  'use strict';

  var module = require('./module');

  module.config(configureStates);

  //---

  configureStates.$inject = ['$stateProvider', '$urlRouterProvider'];

  function configureStates($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('qrcode', {
        url: '/qrcode',
        views: {
          'master': {
            templateUrl   : 'app/core/layout/templates/master.html'
          },
          'content@qrcode': {
            templateUrl   : 'app/core/qrcode/templates/qrcode.html',
            controller    : 'QRCodeCtrl',
            controllerAs  : 'vm'
          }
        }
      });

  }

});
