define(function(require) {
  'use strict';

  var module = require('./module');

  module.config(configureStates);

  //---

  configureStates.$inject = ['$stateProvider', '$urlRouterProvider'];

  function configureStates($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('aluno', {
        url: '/aluno',
        views: {
          'master': {
            templateUrl   : 'app/core/layout/templates/master.html'
          },
          'content@aluno': {
            templateUrl   : 'app/core/aluno/templates/aluno.html',
            controller    : 'AlunoCtrl',
            controllerAs  : 'vm'
          }
        }
      });

  }

});
